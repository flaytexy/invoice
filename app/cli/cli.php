<?php

error_reporting(E_ALL);

declare(ticks=1);

define('APP_PATH',      realpath(__DIR__ . '/../..') . '/');
define('LIB_PATH',      APP_PATH . 'common/lib/');
define('MODELS_PATH',   APP_PATH . 'common/models/');
define('CONFIG_PATH',   APP_PATH . 'common/config/');
define('TEMP_PATH',     APP_PATH . 'temp/');
define('PIDS_PATH',     __DIR__ . '/pids/');

class CliApplication
{
    protected function _registerServices()
    {
        # Register config
        require_once(CONFIG_PATH . 'config.php');

        $loader = new \Phalcon\Loader();

        # Register tasks
        $loader->registerDirs(array(
            APP_PATH . 'app/cli/tasks/',
        ));

        # Register common namespaces
        $loader->registerNamespaces(array(
            'App\Models' => MODELS_PATH,
            'App\Lib' => LIB_PATH,
        ));

        $loader->register();


        $di = new \Phalcon\DI\FactoryDefault\CLI();

        require_once(CONFIG_PATH . 'services.php');

        return $di;
    }

    public function __construct()
    {
        global $argv;

        # Require composer autoload
        // require_once APP_PATH . 'vendor/autoload.php';

        $console = new \Phalcon\CLI\Console();
        $di = $this->_registerServices();

        $arguments = array();
        $params    = array();

        foreach($argv as $k => $arg) {
            if($k == 1) {
                $arguments['task'] = $arg;
            } elseif($k == 2) {
                $arguments['action'] = $arg;
            } elseif($k >= 3) {
                $params[] = $arg;
            }
        }

        $verbose = false;
        if(count($params) > 0) {
            if (in_array('-v', $params))
                $di->setShared('verbose', function(){
                    return true;
                });

            $arguments['params'] = $params;
        }

        $console->setDI($di);

        define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
        define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

        if (!in_array('--concurrent', $params)) {
            $this->_checkPid();
        }

        try {
            $console->handle($arguments);
        } catch (\Phalcon\Exception $e) {
            echo $e->getMessage();
            exit(255);
        }
    }

    protected function _checkPid()
    {
        # Check file lock
        if (!is_readable(PIDS_PATH) || !is_writable(PIDS_PATH)) {
            die(PIDS_PATH . ' IS NOT READABLE! Cannot PROCEED'. PHP_EOL);
        }

        $lock = PIDS_PATH . md5(strtolower(CURRENT_TASK . CURRENT_ACTION)) . '.lock';
        if (file_exists($lock)) {
            die('Script is already running'.PHP_EOL);
        } else {
            file_put_contents($lock, getmypid() . PHP_EOL . date('F j, Y, g:i a'));

            # Remove pid on shutdown
            register_shutdown_function(function() use($lock){
                unlink($lock);
            });

            # Use custom callback for ctrl+c
            pcntl_signal(SIGINT, function(){
                exit;
            });
        }
    }
}

$application = new CliApplication();