<?php
/**
 * Created by PhpStorm.
 * User: ETREX
 * Date: 25.05.2016
 * Time: 12:52
 */

namespace App\Frontend\Controllers;

use App\Lib\Invoice\Exception;
use App\Lib\Invoice\Type\Single;
use App\Lib\Invoice\Type\Multi;
use App\Lib\Invoice\Type\Exchange;


class ApiController extends ControllerBase
{
    public function indexAction()
    {
        exit;
    }

    ////////////////////////////////////////////////////
    // Single
    public function setSingleAction()
    {
        $post = $this->request->getPost();
        //$post = $this->request->get();

        if(!empty($post) && is_array($post)){
            $single = new Single();
            $single->account_id = $post['account_id'];
            $single->amount = $post['amount'];
            $single->expire = $post['expire'];

            try {
                $single->validateNew();
                $single->created = time();
                if($single->_id = $this->findInviteId()){
                    if ($single->save() == false) {
                        if ($debug) {
                            foreach ($single->getMessages() as $message) {
                                echo "Message: ", $message->getMessage();
                                echo "Field: ", $message->getField();
                                echo "Type: ", $message->getType();
                            }
                        }
                    }
                    else{
                        $message =  "The invoice id is: " . $single->getId() ."<br />";
                    }
                }

            } catch (\Exception $e) {
                $error = 'Выброшено исключение: '.  $e->getMessage(). "\n";
            }
        }


        $this->view->message = $message;
        $this->view->error = $error;
    }

    public function getSingleAction()
    {

    }

    public function findInviteId(){
        $invite_id = rand(100000,100000);

        $single->_id = $invite_id;

        $row = Single::findFirst(
            array(
                "conditions" => array(
                    "_id" => (int) $single->_id
                )
            )
        );

        if($row->_id){
            $invite_id = findInviteId();
        }

        return $invite_id;
    }


    ////////////////////////////////////////////////////
    // Multi
}

