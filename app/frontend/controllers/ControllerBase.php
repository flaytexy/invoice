<?php

namespace App\Frontend\Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    protected $title;

    public function beforeExecuteRoute()
    {
        $this->assets
            ->addCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', true)
            ->addJs('https://code.jquery.com/jquery-2.1.4.min.js', true)
            ->addJs('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', true);
    }

    public function afterExecuteRoute()
    {
        $this->tag->appendTitle((!empty($this->title)?  $this->title . ' | ' : '') . $this->config->project->title);

        $this->view->production  = $this->config->project->production;
        $this->view->meta        = '';
    }

    protected function maybeForward($controller, $action)
    {
        if ($this->dispatcher->getControllerName() != $controller || $this->dispatcher->getActionName() != $action) {
            return $this->dispatcher->forward(['controller' => $controller, 'action' => $action]);
        }
    }
}