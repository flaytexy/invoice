<?php
$config = new \Phalcon\Config([
    "project" => [
        'production'     => 0,
        'log'            => 1,
        'title'          => 'Iam.dk',
        'admin_name'     => 'Iam.dk',
        'admin_email'    => 'no-reply@iam.dk',
        'upload_dir'     => APP_PATH . 'public/uploads',
        'log_path'       => APP_PATH . 'logs/debug.log',
        'crypt_key'      => 'kQJHN3axcT128602_xUAH3Nyzc63mi8z'
    ],
    "modules" => [
        'frontend'
    ],
    /*"cache" => [
        'host'           => 'localhost',
        'port'           => '11211',
        'ttl'            => 1200,
        'key'            => 'iamdk'
    ],*/
    "db" => [
        "adapter"        => "Mysql",
        "host"           => "localhost",
        "username"       => "root",
        "password"       => "root",
        "name"           => "some_db",
    ],
    "smtp" => [
        'host'           => 'smtp.gmail.com',
        'port'           => '465',
        'security'       => 'ssl',
        'username'       => 'login',
        'password'       => 'pwd',
    ],
]);