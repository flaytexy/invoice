<?php
/**
 * Created by PhpStorm.
 * User: ETREX
 * Date: 25.05.2016
 * Time: 14:07
 */

namespace App\Lib\Invoice;


class Exception extends \Exception
{
    const ACCOUNT_NOT_VALIDATE    = 100;
    
    const AMOUNT_NOT_VALIDATE      = 101;
    const EXPIRE_NOT_VALIDATE  = 102;

    private $_errors = [
        self::ACCOUNT_NOT_VALIDATE   => 'Param - account_id is wrong',

        self::AMOUNT_NOT_VALIDATE     => 'Param - amount is wrong',
        self::EXPIRE_NOT_VALIDATE => 'Param - expire is wrong'
    ];

    public function __construct($err_code = null)
    {
        if (empty($err_code) || !array_key_exists($err_code, $this->_errors)) {
            return parent::__construct('Unknown error', $err_code);
        }

        return parent::__construct($this->_errors[$err_code], $err_code);
    }
}