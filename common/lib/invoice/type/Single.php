<?php
/**
 * Created by PhpStorm.
 * User: ETREX
 * Date: 25.05.2016
 * Time: 12:42
 */

namespace App\Lib\Invoice\Type;
use App\Lib\Invoice\Exception;
use Phalcon\Mvc\Collection;

class Single extends Collection
{
    public $account_id = '';

    public $amount = '';

    public $expire = '';

    public function initialize()
    {
        $this->useImplicitObjectIds(false);
    }

    public function validateNew(){

        if($this->account_id){
            $account_id = trim(strtoupper($this->account_id));

            if (!preg_match('/^G[0-9A-Z]{55}$/', $account_id)) {
                //$this->logger->error('Invalid accountID ' . $account_id);
                //return null;
                throw new Exception(Exception::ACCOUNT_NOT_VALIDATE);
            }
        }else{
            throw new Exception(101);
        }

        if(!(intval($this->amount) && is_integer(intval($this->amount)))){
            throw new Exception(101);
        }

        if(!($this->is_timestamp($this->expire) && $this->expire > time())){
                throw new Exception(102);
        }

        return true;
    }

    private function is_timestamp($timestamp) {
        if(ctype_digit($timestamp) && strtotime(date('d-m-Y H:i:s',$timestamp)) === (int)$timestamp) {
            return $timestamp;
        } else
            return false;
    }

}